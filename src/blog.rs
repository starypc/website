use rocket::http::ContentType;
use rocket_dyn_templates::{Template, context};
use serde::{Deserialize, Serialize};
use std::fs;
use chrono::{Local, TimeZone};

#[derive(Deserialize)]
struct Post {
    title: String,
    author: String,
    timestamp: i64,
    id: String
}

fn get_posts() -> Vec<Post> {
    serde_json::from_str(&fs::read_to_string("blog/index.json").unwrap()).unwrap()
}

fn get_post_content(id: &str) -> String {
    fs::read_to_string("blog/".to_string() + id + ".html").unwrap()
}

#[get("/blog")]
pub fn blog() -> Template {
    #[derive(Serialize)]
    struct ProcessedPost {
        title: String,
        author: String,
        id: String,
        timestamp: String,
        content: String
    }

    let mut processed_posts = Vec::new();
    for post in get_posts() {
        processed_posts.push(ProcessedPost {
            title: post.title,
            author: post.author,
            content: get_post_content(&post.id),
            id: post.id,
            timestamp: Local.timestamp(post.timestamp, 0)
                .format("%d.%m.%Y %H:%M:%S")
                .to_string()
        });
    }

    Template::render("pages/blog", context! { posts: processed_posts })
}

#[get("/blog/atom")]
pub fn atom_feed() -> (ContentType, Template) {
    #[derive(Serialize)]
    struct ProcessedPost {
        title: String,
        author: String,
        unique_uri: String,
        timestamp: String,
        content: String
    }

    let mut processed_posts = Vec::new();
    let mut last_update = None;
    for post in get_posts() {
        let dt = Local.timestamp(post.timestamp, 0);

        if last_update == None {
            last_update = Some(dt);
        }
        
        processed_posts.push(ProcessedPost {
            title: post.title,
            author: post.author,
            unique_uri: format!("tag:stary.pc.pl,{}:blog.{}", dt.format("%Y-%m-%d").to_string(), post.id),
            timestamp: dt.to_rfc3339(),
            content: get_post_content(&post.id)
        });
    }

    (
        ContentType::new("application", "atom+xml;charset=UTF-8"),
        Template::render("atom_feed", context! {
            last_update: last_update
                .unwrap_or(Local.timestamp(0, 0))
                .to_rfc3339(),
            posts: processed_posts
        })
    )
}
