# stary.pc.pl

This repository contains all the files required for hosting the starypc's homepage, [stary.pc.pl](https://stary.pc.pl/).

## Licensing

See [COPYING](COPYING).

The project is licensed under [GNU GPL 3.0](LICENSES/GPL_3.0). The contents of the website are available under the [CC BY-SA 4.0](LICENSES/CC_BY-SA_4.0) license.
