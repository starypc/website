mod blog;

#[macro_use] extern crate rocket;

use rocket::{Request, fs::FileServer};
use rocket_dyn_templates::{Template, context};
use std::process::Command;

#[get("/")]
fn index() -> Template {
    Template::render("pages/index", context! {})
}

#[get("/status")]
async fn status() -> Template {
    let tuptime = match Command::new("tuptime").output() {
        Ok(out) => String::from_utf8(out.stdout).unwrap(),
        Err(_) => String::new(),
    };
    let ip_address = reqwest::get("https://api.ipify.org/")
        .await
        .unwrap()
        .text()
        .await
        .unwrap();

    Template::render("pages/status", context! { tuptime, ip_address })
}

#[get("/contact")]
fn contact() -> Template {
    Template::render("pages/contact", context! {})
}

#[get("/credits")]
fn credits() -> Template {
    Template::render("pages/credits", context! {})
}

#[get("/localhost_tls")]
fn localhost_tls() -> Template {
    Template::render("pages/localhost_tls", context! {})
}

#[catch(404)]
fn error_404(_req: &Request) -> Template {
    Template::render("errors/404", context! {})
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .register("/", catchers![error_404])
        .mount("/", routes![index, blog::blog, blog::atom_feed, status, contact, credits, localhost_tls])
        .mount("/", FileServer::new("files", rocket::fs::Options::DotFiles))
        .attach(Template::fairing())
}
